package main

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

// how are you supposed to represent filters?
// structs probably aren't types...

type AmiFilter struct {
	VirtualizationType string // not sure how to represent enum hvm | arm
	Architecture string
	Name string
	BlockDeviceMapping struct {
		VolumeType string
	}
}

type Builder struct {
	AmiName string
	AmiDescription string
	InstanceType string
	Name string
	Region string
	Type string
	SourceAmiFilter AmiFilter 
}


func packerFile() *schema.Resource {
	return &schema.Resource{
		Create: resourcePackerFileCreate,
		Read: resourcePackerFileRead,
		Update: resourcePackerFileUpdate,
		Delete: resourcePackerFileDelete,

		Schema: map[string]*schema.Schema{
			"variables": &schema.Schema{
				Type: schema.TypeMap,
				Required: true,
			},
			"builders": &schema.Schema{},
			"provisioners": &schema.Schema{},
		},
	}
}

func resourcePackerFileCreate(d *schema.ResourceData, m interface{}) error {
	return resourcePackerFileRead(d,m)
}

func resourcePackerFileRead(d *schema.ResourceData, m interface{}) error {
	return nil
}

func resourcePackerFileUpdate(d *schema.ResourceData, m interface{}) error {
	return resourceServerRead(d, m)
}

func resourcePackerFileDelete(d *schema.ResourceData, m interface{}) error {
	return nil
}


// since a fully formed representation is about out of my depth
// let's just fiddle with an existing representation