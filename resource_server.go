
package main


import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"

)



func resourceServer() *schema.Resource {
	return &schema.Resource{
		Create: resourceServerCreate,
		Read: resourceServerRead,
		Update: resourceServerUpdate,
		Delete: resourceServerDelete,

		Schema: map[string]*schema.Schema{
			"address": &schema.Schema{
				Type: schema.TypeString,
				Required: true,
			},
		},
	}
}


func resourceServerCreate(d *schema.ResourceData, m interface{}) error {
	// schema.ResourceData.Get("address")
	address := d.Get("address").(string)
	// non-blank ID => tells terraform that a resource was created
	// ID => any string value (but should be a value that can be used to read the resource again)
	d.SetId(address)
	return resourceServerRead(d, m)
}

func resourceServerRead(d *schema.ResourceData, m interface{}) error {
	client := m.(*AClient)

	// attempt to read from an upstream API
	obj, ok := client.Get(d.Id())

	// if the resource does not exist, inform Terraform
	// We want to immediately return here to prevent further processing
	if !ok {
		d.SetId("")
		return nil
	}
	d.Set("address", obj.Address)
	return nil
}

func resourceServerUpdate(d *schema.ResourceData, m interface{}) error {
	// Enable partial state mode
	d.Partial(true)

	if d.HasChange("address") {
		if err := updateAddress(d, m); err != nil {
			return err
		}

		d.SetPartial("address")
	}
	// Disable partial mode on success.
	// What about on error?
	d.Partial(false)

	return resourceServerRead(d, m)
}

func resourceServerDelete(d *schema.ResourceData, m interface{}) error {
	// "It is not necessary to call d.SetId("") since any non-error return value assumes the resource was deleted successfully
	d.SetId("")
	// should always handle the case where the resource might already be destroyed (manually, for example)
	// if the resource is already destroyed, this should not return an error
	return nil
}


// Field writer?
func updateAddress(d *schema.ResourceData, m interface{}) error {
	return nil
}